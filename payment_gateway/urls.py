from django.urls import path
from .views import donation_list, get_donation_param_doku, notify_doku
from .views import distribution_list, create_distribution_doku, remit_doku

app_name = "payment_gateway"

urlpatterns = [
	path('donation-list.abs', donation_list),
	path('donation-param-doku.abs', get_donation_param_doku),
	path('notify-doku.abs', notify_doku),
	path('distribution-list.abs', distribution_list),
	path('distribution-doku.abs', create_distribution_doku),
	path('remit-doku.abs', remit_doku)
]
