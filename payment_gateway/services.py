import datetime
import requests
import json
import uuid
from hashlib import sha1
from .helper import AESCipher
from .params import get_doku_donation_param_api, get_doku_distribution_param_api
from .models import Donation, Income, Distribution
from django.core import serializers

def get_all_donations():
	donation_objects = list()
	donations = Donation.objects.all()
	for donation in donations:
		donation_objects.append({
			'id': donation.pk,
			'name': donation.name,
			'email': donation.email,
			'phone': donation.phone,
			'amount': donation.amount,
			'paymentgateway': donation.paymentgateway,
			'requestdatetime': donation.requestdatetime,
			'trxstatus': donation.trxstatus,
			'transidmerchant': donation.transidmerchant,
			'words': donation.words,
			'incomeid': donation.incomeid.pk if donation.incomeid else 0
		})
	return {"data": donation_objects}
	
def get_all_distributions():
	distribution_objects = list()
	distributions = Distribution.objects.all()
	for distribution in distributions:
		distribution_objects.append({
			'id': distribution.pk,
			'name': distribution.name,
			'email': distribution.email,
			'phone': distribution.phone,
			'amount': distribution.amount,
			'paymentgateway': distribution.paymentgateway,
			'requestdatetime': distribution.requestdatetime,
			'trxstatus': distribution.trxstatus,
			'inquiryid': distribution.inquiryid
		})
	return {"data": distribution_objects}

def generate_donation_param_doku(request):
	if request.method != "POST": return non_post_msg()
	
	name = request.POST["name"]
	email = request.POST["email"]
	phone = request.POST["phone"]
	amount = request.POST["amount"]
	
	env_doku = get_doku_donation_param_api()
	
	shared_key = env_doku["SHARED_KEY"]
	mallid = env_doku["MALLID"]
	doku_endpoint = env_doku["ENDPOINT"]
	
	timestamp = datetime.datetime.now().strftime("%Y.%m.%d.%H.%M.%S.%f")
	hashed_timestamp = sha1(timestamp.encode()).hexdigest()
	min_timestamp = hashed_timestamp[:10]
	dateonly = datetime.datetime.now().strftime("%Y%m%d")
	transaction_id = "AISCO-" + dateonly + "-" + min_timestamp
	session_id = transaction_id
	total_cost = amount
	total_cost_str = str(amount) + ".00"
	word_to_hash = total_cost_str+mallid+shared_key+transaction_id
	words = sha1(word_to_hash.encode()).hexdigest()
	date_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
	basket_str = "Donation,"+total_cost_str+",1,"+total_cost_str
	
	Donation.objects.create(name=name, email=email, phone=phone, amount=total_cost, 
	requestdatetime=date_time, mallid=mallid, transidmerchant=transaction_id, 
	words=words, sessionid=session_id, basket=basket_str, currency="360", 
	purchasecurrency="360", chainmerchant="NA", paymentgateway="DOKU", trxstatus="PENDING")
	
	result = {
		"ENDPOINT": doku_endpoint,
		"MALLID": mallid,
		"CHAINMERCHANT": "NA",
		"AMOUNT": total_cost_str,
		"PURCHASEAMOUNT": total_cost_str,
		"TRANSIDMERCHANT": transaction_id,
		"WORDS": words,
		"REQUESTDATETIME": date_time,
		"CURRENCY": "360",
		"PURCHASECURRENCY": "360",
		"SESSIONID": "session_id",
		"NAME": name,
		"EMAIL": email,
		"BASKET": basket_str,
		"MOBILEPHONE": phone
	}
	
	data_result = {"data": result}
	return data_result

def handle_notify_doku(request):
	if request.method != "POST": return non_post_msg()
	
	incoming_words = request.POST["WORDS"]
	amount = request.POST["AMOUNT"]
	transidmerchant = request.POST["TRANSIDMERCHANT"]
	approvalcode = request.POST["APPROVALCODE"]
	trxstatus = request.POST["RESULTMSG"]
	verifystatus = request.POST["VERIFYSTATUS"]
	
	print(transidmerchant)
	
	env_doku = get_doku_donation_param_api()
	
	shared_key = env_doku["SHARED_KEY"]
	mallid = env_doku["MALLID"]
	
	word_to_hash = amount+mallid+shared_key+transidmerchant+trxstatus+verifystatus
	calculated_words = sha1(word_to_hash.encode()).hexdigest()
	
	result_message = ""
	debug = True
	if incoming_words == calculated_words or debug:
		donation = Donation.objects.get(transidmerchant=transidmerchant)
		donation.trxstatus = trxstatus
		if trxstatus == "SUCCESS":
			name = donation.name
			description = "Donation from " + name + " with Payment Gateway DOKU"
			date_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
			
			income = Income.objects.create(datestamp=date_time, description=description, amount=int(amount))
			
			donation.incomeid = income
		donation.save()
		result_message = trxstatus
	else:
		result_message = "Invalid request"
		
	return {"data": result_message}
	
def generate_distribution_doku(request):
	if request.method != "POST": return non_post_msg()
	
	name = request.POST["name"]
	email = request.POST["email"]
	phone = request.POST["phone"]
	amount = request.POST["amount"]
	
	account_number = request.POST["account_number"]
	reciever_city = request.POST["reciever_city"]
	bank_id = request.POST["bank_id"]
	bank_swift_code = request.POST["bank_swift_code"]
	bank_name = request.POST["bank_name"]
	
	env_doku = get_doku_distribution_param_api()
	
	agent_key = env_doku["AGENT_KEY"]
	encryption_key = env_doku["ENCRYPTION_KEY"]
	request_id = "fd53b1e3-9c35-4dfa-a95c-a8dbc5695485"
	
	signature = "c7gWWl1UEDpkO42/Ikqjd4G2crci/iaqzAylLzkQC0Ua94iF+IAv/wbhjWnwdcc9"
	
	endpoint = env_doku["ENDPOINT_INQUIRY"]
	header = {
		"Content-Type": "application/json",
		"requestId": request_id,
		"agentKey": agent_key,
		"signature": signature,
	}
	
	payload = {	
		"senderCountry": {"code": "ID"},
		"senderCurrency": {"code": "IDR"},
		"beneficiaryCountry": {"code": "ID"},
		"beneficiaryCurrency": {"code": "IDR"},
		"channel": {"code": "07"},
		"senderAmount": amount,
		"beneficiaryAccount": {
			"bank": {
				"code": bank_swift_code,
				"countryCode": "ID",
				"id": bank_id,
				"name": bank_name
			},
			"city": reciever_city,
			"name": name,
			"number": account_number
		}
	}
	
	print(payload)
	
	resp = requests.post(endpoint, headers=header, json=payload)
	inquiry_response = resp.json()
	
	print(inquiry_response)
	
	status = inquiry_response["status"]
	msg = inquiry_response["message"]
	
	inquiry_id = "00000"
	if status == "0" or status == 0:
		date_time = date_time = datetime.datetime.now().strftime("%Y-%m-%d")
		inquiry_id = inquiry_response["inquiry"]["idToken"]
		
		Distribution.objects.create(name=name, email=email, phone=phone, amount=amount, 
		requestdatetime=date_time, paymentgateway="DOKU", trxstatus="PENDING", 
		inquiryid=inquiry_id, accountnumber=account_number, bankid=bank_id, bankname=bank_name, 
		bankswiftcode=bank_swift_code, recievercity=reciever_city)
	
	result = {
		"status": status,
		"message": msg,
		"idToken": inquiry_id
	}
	
	return {"data": result}

def handle_remit_doku(request):
	if request.method != "POST": return non_post_msg()
	
	inquiry_id = request.POST["inquiry_id"]
	
	distribution = Distribution.objects.get(inquiryid=inquiry_id)
	
	name = distribution.name
	email = distribution.email
	phone = distribution.phone
	amount = distribution.amount
	
	account_number = distribution.accountnumber
	reciever_city = distribution.recievercity
	bank_id = distribution.bankid
	bank_swift_code = distribution.bankswiftcode
	bank_name = distribution.bankname
	
	env_doku = get_doku_distribution_param_api()
	
	agent_key = env_doku["AGENT_KEY"]
	encryption_key = env_doku["ENCRYPTION_KEY"]
	request_id = "fd53b1e3-9c35-4dfa-a95c-a8dbc5695485"
	
	signature = "c7gWWl1UEDpkO42/Ikqjd4G2crci/iaqzAylLzkQC0Ua94iF+IAv/wbhjWnwdcc9"
	
	endpoint = env_doku["ENDPOINT_REMIT"]
	header = {
		"Content-Type": "application/json",
		"requestId": request_id,
		"agentKey": agent_key,
		"signature": signature,
	}
	payload = {	
		"senderCountry": {"code": "ID"},
		"senderCurrency": {"code": "IDR"},
		"beneficiaryCountry": {"code": "ID"},
		"beneficiaryCurrency": {"code": "IDR"},
		"inquiry": {"idToken": inquiry_id},
		"channel": {"code": "07"},
		"senderAmount": amount,
		"beneficiaryAccount": {
			"bank": {
				"code": bank_swift_code,
				"countryCode": "ID",
				"id": bank_id,
				"name": bank_name
			},
			"city": reciever_city,
			"name": name,
			"number": account_number
		},
		"beneficiary": {
			"country": {"code": "ID"},
			"firstName": name,
			"lastName": name,
			"phoneNumber": phone
		},
		"beneficiaryCity": "",
		"sender": {
			"birthDate": "2020-05-21",
			"country": {"code": "ID"},
			"firstName": "AISCO",
			"lastName": "Microservice",
			"personalId": "000000000000",
			"personalIdType": "KTP",
			"personalIdCountry": {"code": "ID"},
			"phoneNumber": "000011112222"
		}
	}
	
	resp = requests.post(endpoint, headers=header, json=payload)
	remit_response = resp.json()
	
	status = remit_response["status"]
	msg = remit_response["message"]
	
	transaction_id = "00000"
	transaction_status = "Error"
	
	if status == "0" or status == 0:
		transaction_status = remit_response["remit"]["transactionStatus"]
		transaction_id = remit_response["remit"]["transactionId"]
		if transaction_status == "50" or transaction_status == 50:
			distribution = Distribution.objects.get(inquiryid=inquiry_id)
			distribution.trxstatus = "SUCCESS"
			distribution.save()
		elif transaction_status == "20" or transaction_status == 20:
			distribution = Distribution.objects.get(inquiryid=inquiry_id)
			distribution.trxstatus = "FAILED"
			distribution.save()
	
	result = {
		"status": status,
		"message": msg,
		"transaction_status": transaction_status,
		"transaction_id": transaction_id
	}
	
	return {"data": result}
	
def non_post_msg():
	error_payload = {"success": False, "message": "Request must be POST"}
	return {"data": error_payload}