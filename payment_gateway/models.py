# -*- coding: utf-8 -*
from __future__ import unicode_literals
import uuid

from django.db import models

# Create your models here.
class Income(models.Model):
	datestamp = models.CharField(max_length=100, blank=True, null=True)
	description = models.CharField(max_length=100, blank=True, null=True)
	amount = models.IntegerField(blank=True, null=True)
	programName = models.CharField(max_length=100, blank=True, null=True)
	
class Donation(models.Model):
	name = models.CharField(max_length=100, blank=True, null=True)
	email = models.CharField(max_length=100, blank=True, null=True)
	phone = models.CharField(max_length=100, blank=True, null=True)
	amount = models.CharField(max_length=100, blank=True, null=True)
	requestdatetime = models.CharField(max_length=100, blank=True, null=True)
	accountnumber = models.CharField(max_length=100, blank=True, null=True)
	paymentgateway = models.CharField(max_length=100, blank=True, null=True)
	trxstatus = models.CharField(max_length=100, blank=True, null=True)
	mallid = models.CharField(max_length=100, blank=True, null=True)
	chainmerchant = models.CharField(max_length=100, blank=True, null=True)
	purchaseamount = models.CharField(max_length=100, blank=True, null=True)
	transidmerchant = models.CharField(max_length=100, blank=True, null=True)
	words = models.CharField(max_length=100, blank=True, null=True)
	currency = models.CharField(max_length=100, blank=True, null=True)
	purchasecurrency = models.CharField(max_length=100, blank=True, null=True)
	sessionid = models.CharField(max_length=100, blank=True, null=True)
	basket = models.CharField(max_length=100, blank=True, null=True)
	approvalcode = models.CharField(max_length=100, blank=True, null=True)
	incomeid = models.ForeignKey(Income,on_delete=models.CASCADE, blank=True, null=True)
	
class Distribution(models.Model):
	name = models.CharField(max_length=100, blank=True, null=True)
	email = models.CharField(max_length=100, blank=True, null=True)
	phone = models.CharField(max_length=100, blank=True, null=True)
	amount = models.CharField(max_length=100, blank=True, null=True)
	requestdatetime = models.CharField(max_length=100, blank=True, null=True)
	accountnumber = models.CharField(max_length=100, blank=True, null=True)
	paymentgateway = models.CharField(max_length=100, blank=True, null=True)
	trxstatus = models.CharField(max_length=100, blank=True, null=True)
	inquiryid = models.CharField(max_length=100, blank=True, null=True)
	bankid = models.CharField(max_length=100, blank=True, null=True)
	bankname = models.CharField(max_length=100, blank=True, null=True)
	bankswiftcode = models.CharField(max_length=100, blank=True, null=True)
	recievercity = models.CharField(max_length=100, blank=True, null=True)